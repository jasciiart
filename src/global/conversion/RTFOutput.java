/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.Info;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class provides RTF output format support.
 * @author xsdnyd
 */
public class RTFOutput extends TextOutput {

    /** This marks the placement of the colortable in the file. */
    private final String colorTableMark = "#COLORTABLE#";
    /** 
     * This is the header of the resulting RTF file. It has some variables
     * for fontname, fontsize etc. These are filled in later.
     */
    private String header = "{\\rtf\\ansi\\deff0\\paperw%3$d\\paperh%4$d" +
            "\\margl0\\margr0\\margt0\\margb0%5$s\\viewzk1" +
            "{\\fonttbl{\\f0\\fnil\\fcharset0 %1$s;}}%n%2$s%n";
    /** This string represents a new line in the RTF file. */
    private final String newLine = "\\cf0\\par";
    /** This is the footer of the RTF file. */
    private String footer = "}";
    /** The fontsize of the resulting RTF ascii picture. */
    private int fontSize;
    /** The background color of the RTF file. */
    private Color backColor;
    /** 
     * This variable stores the last color. To determine wether a new
     * color tag is needed (to change output to the new color).
     */
    private int oldColor;
    /**
     * This variable stores the current color count. The colortable holds all
     * colors to which the output text refers with the index of the color in the 
     * table.
     */
    private int colorCount;
    /** The colortable which stores the index of the color. */
    private HashMap<Integer, Integer> colorTable;
    /** This list stores the actual colors and later replaces the colortable mark. */
    private ArrayList<IntColor> list;

    /**
     * The constructor just initializes some variables and formats the header
     * with the specified values.
     * The target width and height is calculated.
     * @param font The output font.
     * @param dataProvider The provider of the data.
     * @param backColor The background color of the ascii picture.
     * @param img The input image. Used to determine font width and image width.
     */
    public RTFOutput(Font font, DataProvider dataProvider, Color backColor,
            BufferedImage img) {
        final int TWIPS_PER_PIXEL = 20;
        this.fontSize = font.getSize();
        this.dataProvider = dataProvider;
        this.backColor = backColor;
        colorTable = new HashMap<Integer, Integer>(2000);
        oldColor = -1;
        colorCount = 0;
        list = new ArrayList<IntColor>();
        FontMetrics fm = img.getGraphics().getFontMetrics(font);
        int max = 0;
        for (int w : fm.getWidths())
            if (w > max)
                max = w;
        int width = (max + 3) * img.getWidth() * TWIPS_PER_PIXEL;
        int height = (fm.getMaxAscent() + fm.getMaxDescent()) *
                img.getHeight() * TWIPS_PER_PIXEL;
        header = String.format(header, font.getName(), colorTableMark, width,
                height, width > height ? "\\landscape" : "");
        if (font.isBold()) {
            header = header + "\\b";
            footer = "\\b0\n" + footer;
        }
    }

    /**
     * Returns the already formatted header string. The header string is 
     * formatted in the constructor.
     * @return Formatted header string.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Returns the footer.
     * @return Footer.
     */
    public String getFooter() {
        return footer;
    }

    /**
     * In the RTF output file format some chars are not allowed because they
     * belong to the syntax. We repeat the data generation until we get some
     * data which does not equals these chars.
     * @param red The red color component.
     * @param green The green color component.
     * @param blue The blue color component.
     * @return The valid ascii data.
     */
    public final String processData(int red, int green, int blue) {
        String str = null;
        boolean correct = false;
        while (!correct) {
            str = dataProvider.getData(red, green, blue);
            for (char c : str.toCharArray())
                if (c == '\\' || c == '}' || c == '{' || c == ';') {
                    correct = false;
                    break;
                } else
                    correct = true;
        }
        return str;
    }

    /**
     * This function processes each pixel. If the color is not equal to the old
     * one, the color is looked up in the table. If the color is not in the 
     * table it is added to it. The function then returns the ascii data.
     * If the color is equal to the old color, only the ascii data is returned.
     */
    public String processPixel(int pixel, int red, int green, int blue,
            int color) {
        if (color != oldColor) {
            oldColor = color;
            Integer c = new Integer(color);
            if (colorTable.containsKey(c))
                return "\\cf" + colorTable.get(c).intValue() + " " + processData(red, green, blue);
            else {
                colorCount++;
                colorTable.put(c, new Integer(colorCount));
                list.add(new IntColor(red, green, blue));
                return "\\cf" + colorCount + " " + processData(red, green, blue);
            }
        } else
            return processData(red, green, blue);
    }

    /**
     * When a new line is reached the color tags needs to be repeated.
     * The function returns the RTF specific new line string.
     * @return Newline string.
     */
    public String getNextLineString() {
        oldColor = -1;
        return newLine + Info.LINE_END;
    }

    /**
     * To create a valid RTF file we need to post process the colortable.
     * First the background color is searched in the table. If not it is added.
     * Then all colors are added to the output colortable.
     * The processed colortable replaces the colortable mark in the RTF output 
     * string.
     * Since the fontsize in a RTF file is specified in half-points we have
     * to multiply out fontsize with 2.
     * The space between each line is pretty big, so we set the value to a
     * negative one. For this we tested some values and found out that
     * a value of fontsize * 20 is pretty good and provides a tiny space between
     * two lines.
     * @param buf The RTF output file string.
     */
    public void postProcess(StringBuffer buf) {
        StringBuffer table = new StringBuffer("{\\colortbl ");
        Integer background = new Integer(backColor.getRGB());
        int pos = -1;
        if (colorTable.containsKey(background))
            pos = colorTable.get(background) + 1;
        else {
            list.add(new IntColor(backColor.getRed(), backColor.getGreen(),
                    backColor.getBlue()));
            pos = colorCount + 1;
        }
        for (IntColor ic : list)
            table.append(String.format(";\\red%d\\green%d\\blue%d",
                    ic.getRed(), ic.getGreen(), ic.getBlue()));
        table.append(String.format("%s;}\\qc\\sl-%d\\expnd-1\\fs%d\\cbpat%d",
                Info.LINE_END, fontSize * 20, fontSize * 2, pos));
        pos = buf.indexOf(colorTableMark);
        buf.replace(pos, pos + colorTableMark.length(), table.toString());
        colorTable.clear();
        list.clear();
    }

    /**
     * Since the colortable needs to be post processed and embedded in the
     * output file, we can't flush the output while the processing is still 
     * running. Hopefully we can change this some time.
     * So this function does nothing.
     * @param bw Not used.
     * @param data Not used.
     * @throws java.io.IOException Never thrown.
     */
    public void flush(BufferedWriter bw, StringBuffer data) throws IOException {
    }
}
