/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import java.awt.Color;

/**
 * This class just stores a RGB color in its three color components.
 * It also provides the function to convert a color to its hexadecimal format.
 * @author xsdnyd
 */
public class IntColor {

    /** The three color components. */
    private int red,  green,  blue;

    /**
     * Create a new IntColor object an initializes the color components.
     * @param red The red color component.
     * @param green The green color component.
     * @param blue The blue color component.
     */
    public IntColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    /**
     * Returns the red color component.
     * @return Red color component.
     */
    public int getRed() {
        return red;
    }

    /**
     * Returns the green color component.
     * @return Green color component.
     */
    public int getGreen() {
        return green;
    }

    /**
     * Returns the blue color component.
     * @return Blue color component.
     */
    public int getBlue() {
        return blue;
    }

    /**
     * Two IntColor object are equal if all three color components are equal.
     * @param ic The IntColor object to which this one is compared.
     * @return True if all color components are equal.
     */
    public boolean equals(IntColor ic) {
        return getRed() == ic.getRed() && getGreen() == ic.getGreen() &&
                getBlue() == ic.getBlue();
    }

    /**
     * This function converts a color to its hexadecimal representation.
     * The output String is always 6 characters long. 2 chars for each color
     * component.
     * @param c The color to convert.
     * @return The hexadecimal representation.
     */
    public static String colorToHex(Color c) {
        return intToHex(c.getRed(), c.getGreen(), c.getBlue());
    }

    /**
     * This function converts a color to its hexadecimal representation.
     * The output String is always 6 characters long. 2 chars for each color
     * component.
     * @param red The red color component.
     * @param green The green color component.
     * @param blue The blue color component.
     * @return The hexadecimal representation.
     */
    public static String intToHex(int red, int green, int blue) {
        return String.format("%02x%02x%02x", red, green, blue);
    }
}
