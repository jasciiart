/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.HTMLEntities;
import global.misc.Info;
import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * This class provides HTML with inline CSS output format support.
 * @author xsdnyd
 */
public class CSSHTMLOutput extends TextOutput {

    /** This map assigns the hexadecimal string to each color. */
    private HashMap<Integer, String> colorTable;
    /** Stores the last hexadecimal color. */
    private int oldColor;
    /** This marks the placement of the colortable in the file. */
    private final String colorTableMark = "#COLORTABLE#";
    /** The tags used to specify the font color. */
    private final String mainTag = "span",  subTag = "class";
    /** The footer of the HTML file. */
    private final String footer = "</span></center></pre></body></html>";
    /** The header of the HTML file. The variables will filled in later. */
    private String header =
            "<html><head><title>ascii color picture</title>%n" +
            "<style type=\"text/css\">%n" +
            "body {background-color: #%1$s; font-size:%2$dpt;%n" +
            "      line-height:%2$dpt; font-family:'%3$s'; " +
            "      font-weight:%4$s;}%n" +
            "%5$s%n" +
            "</style></head>%n" +
            "<body><pre><center>";
    /** HTML entity object to provide the entities. */
    private HTMLEntities entityArray;

    /**
     * The constructor initializes some variables and formats the header and 
     * footer.
     * @param font The output font.
     * @param dataProvider The provider of the data.
     * @param backColor The HTML background color.
     */
    public CSSHTMLOutput(Font font, DataProvider dataProvider, Color backColor) {
        this.dataProvider = dataProvider;
        oldColor = -1;
        entityArray = new HTMLEntities();
        colorTable = new HashMap<Integer, String>(2000);
        header = String.format(header, IntColor.colorToHex(backColor),
                font.getSize(), font.getFontName(),
                font.isBold() ? "bold" : "normal", colorTableMark);
    }

    /**
     * Returns the already formatted HTML header. The header is formatted in the
     * constructor
     * @return HTML header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Returns the HTML footer.
     * @return HTML footer.
     */
    public String getFooter() {
        return footer;
    }

    /**
     * If the old color is equal to the new color the hexadecimal color string
     * is stored in the table and the color tags with the ascii data is
     * returned. If the color is the same as the old color only the ascii data
     * is returned.
     * The ascii data is converted to contain valid HTML entities.
     */
    public String processPixel(int pixel, int red, int green, int blue,
            int color) {
        String processedData = dataProvider.getData(red, green, blue);
        String entityString = "";
        for (int i = 0; i < processedData.length(); i++)
            entityString += entityArray.getEntity(processedData.charAt(i));
        if (oldColor != color) {
            oldColor = color;
            Integer c = new Integer(color);
            String hex = IntColor.intToHex(red, green, blue);
            if (!colorTable.containsKey(c))
                colorTable.put(c, hex);
            return (pixel != 1 ? "</" + mainTag + ">" : "") +
                    String.format("<%s %s=\"c%s\">%s", mainTag, subTag, hex, entityString);
        } else
            return entityString;
    }

    /**
     * Just return a newline String.
     * @return Newline String.
     */
    public String getNextLineString() {
        return Info.LINE_END;
    }

    /**
     * Generate a CSS colortable from the map and replaces the colortable mark
     * in the output string. Afterwards the map is cleared.
     * @param buf The HTML output string.
     */
    public void postProcess(StringBuffer buf) {
        StringBuffer table = new StringBuffer();
        for (String hex : colorTable.values())
            table.append(String.format(".c%1$s{color:#%1$s;}%2$s", hex, Info.LINE_END));
        int pos = buf.indexOf(colorTableMark);
        buf.replace(pos, pos + colorTableMark.length(), table.toString());
        colorTable.clear();
    }

    /**
     * Since the colortable needs to be post processed and embedded in the
     * output file, we can't flush the output while the processing is still 
     * running. Hopefully we can change this some time.
     * So this function does nothing.
     * @param bw Not used.
     * @param data Not used.
     * @throws java.io.IOException Never thrown.
     */
    public void flush(BufferedWriter bw, StringBuffer data) throws IOException {
    }
}
