/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.HTMLEntities;
import global.misc.Info;
import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * This class provides HTML output format support.
 * @author xsdnyd
 */
public class HTMLOutput extends TextOutput {

    /** The footer of the HTML file. */
    private String footer = "</font></center></pre></body></html>";
    /** The header of the HTML file. The variables will filled in later. */
    private String header =
            "<html><head><title>ascii color picture</title>%n" +
            "<style type=\"text/css\">%n" +
            "body {background-color: #%1$s; font-size:%2$dpt;%n" +
            "      line-height:%2$dpt; font-family:'%3$s'; }%n" +
            "</style></head>%n" +
            "<body><pre><center>";
    /** The tags used to specify the font color. */
    private final String mainTag = "font",  subTag = "color";
    /** Stores the last hexadecimal color. */
    private String oldHex;
    /** HTML entity object to provide the entities. */
    private HTMLEntities entityArray;

    /**
     * The constructor initializes the DataProvider and formats the header and 
     * footer.
     * @param font The output font.
     * @param dataProvider The provider of the data.
     * @param backColor The HTML background color.
     */
    public HTMLOutput(Font font, DataProvider dataProvider, Color backColor) {
        this.dataProvider = dataProvider;
        oldHex = "";
        entityArray = new HTMLEntities();
        header = String.format(header, IntColor.colorToHex(backColor),
                font.getSize(), font.getFontName());
        if (font.isBold()) {
            header = header + "<b>";
            footer = "</b>" + footer;
        }
    }

    /**
     * Returns a new colortag and the ascii data if the new color is different 
     * from the previous one. Otherwise just returns the ascii data.
     * The ascii data is converted to contain valid HTML entities.
     */
    public String processPixel(int pixel, int red, int green, int blue,
            int color) {
        String newHex = IntColor.intToHex(red, green, blue);
        String processedData = dataProvider.getData(red, green, blue);
        String entityString = "";
        for (int i = 0; i < processedData.length(); i++)
            entityString += entityArray.getEntity(processedData.charAt(i));
        if (!newHex.contentEquals(oldHex)) {
            oldHex = newHex;
            return (pixel != 1 ? "</" + mainTag + ">" : "") +
                    String.format("<%s %s=\"#%s\">%s", mainTag, subTag, newHex, entityString);
        } else
            return entityString;
    }

    /**
     * Just return a newline String.
     * @return Newline String.
     */
    public String getNextLineString() {
        return Info.LINE_END;
    }

    /**
     * Returns the already formatted HTML header. The header is formatted in the
     * constructor
     * @return HTML header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Returns the HTML footer.
     * @return HTML footer.
     */
    public String getFooter() {
        return footer;
    }

    /** No post processing is done. This function will do nothing. */
    public void postProcess(StringBuffer buf) {
    }

    public void flush(BufferedWriter bw, StringBuffer data) throws IOException {
        bw.write(data.toString());
        data.delete(0, data.length());
    }
}
