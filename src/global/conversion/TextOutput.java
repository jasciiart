/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * This class specifies the required functions for all TextOutput classes.
 * @author xsdnyd
 */
public abstract class TextOutput {

    /**
     * The data provider to use for text output and ascii representation of a 
     * pixel.
     */
    protected DataProvider dataProvider;

    /**
     * If the output text file needs a header, this function has to return
     * something. If the file doesn't need a header, this function should
     * return an empty string. Not null.
     * @return Header of text file (not null).
     */
    public abstract String getHeader();

    /**
     * If the output text file needs a footer, this function has to return
     * something. If the file doesn't need a footer, this function should
     * return an empty string. Not null.
     * @return Footer of text file (not null).
     */
    public abstract String getFooter();

    /**
     * This is the main function of a TextOutput class. Every pixel of the
     * input image is processed using this function. The return value should be
     * the ascii representation of the pixel in the output text file.
     * @param pixel The number of the current pixel. Results via the calculation
     * x * y in the input image. Where x and y are the current positions.
     * @param red The red component of the color.
     * @param green The green component of the color.
     * @param blue The blue component of the color.
     * @param color The RGB color.
     * @return The ascii representation of the pixel (eg. a font tag with some
     * text if the output is a html file).
     */
    public abstract String processPixel(int pixel, int red, int green, int blue,
            int color);

    /**
     * After every scanline, this function is called to add a possible
     * next line string. If not needed, return an empty string.
     * @return Nextline string.
     */
    public abstract String getNextLineString();

    /**
     * If the generated data needs to be postprocessed (eg. for a colortable),
     * the whole data can be manipulated.
     * @param buf
     */
    public abstract void postProcess(StringBuffer buf);

    /**
     * To avoid too much heap usage, the TextOutput classes can write
     * their already generated data to disc. If the string has to be
     * postprocessed, this function can be ignored.
     * @param bw The writer, that writes to the output file.
     * @param data The current data, which can be written.
     * @throws java.io.IOException
     */
    public abstract void flush(BufferedWriter bw, StringBuffer data)
            throws IOException;
}
