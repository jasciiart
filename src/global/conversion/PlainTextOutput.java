/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.Info;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * This class provides plain text output format support.
 * @author xsdnyd
 */
public class PlainTextOutput extends TextOutput {

    /** 
     * This map assigns a character to each color. This way every color has
     * a fixed character.
     */
    private HashMap<Integer, Character> map;

    /**
     * The constructor builds a map of colors to characters.
     * If not enough different characters are available we try to find a
     * last unused character. If we didn't found one after 400 tries we just
     * use the last one tried.
     * @param dataProvider The provider of the data.
     * @param palette The colorpalette.
     */
    public PlainTextOutput(DataProvider dataProvider, int palette[]) {
        this.dataProvider = dataProvider;
        dataProvider.setTwice(false);
        map = new HashMap<Integer, Character>((int) (dataProvider.getMutation() * 
                Info.HASHMAP_LOAD_FACTOR_INVERTED));
        Character c = null;
        if (dataProvider.getMode() != DataProvider.DATA_BRIGHTNESSPATTERN) {
            int tries, mutations = dataProvider.getMutation();
            for (int i = 0; i < palette.length; i++) {
                boolean found = true;
                tries = 0;
                while (found) {
                    c = new Character(dataProvider.getData().charAt(0));
                    found = map.containsValue(c);
                    tries++;
                    if ((palette.length > mutations) && (tries >= 400))
                        break;
                }
                map.put(new Integer(palette[i]), new Character(c));
            }
        }
        dataProvider.setTwice(true);
    }

    /**
     * A plain text file needs no header, so an empty String is returned.
     * @return Empty String.
     */
    public String getHeader() {
        return "";
    }

    /**
     * A plain text file needs no footer, so an empty String is returned.
     * @return Empty String.
     */
    public String getFooter() {
        return "";
    }

    /**
     * If the data mode is set to brightness pattern, the DataProvider can
     * returns valid ascii data.
     * Otherwise lookup the character in the map and return it.
     */
    public String processPixel(int pixel, int red, int green, int blue,
            int color) {
        if (dataProvider.getMode() == DataProvider.DATA_BRIGHTNESSPATTERN)
            return dataProvider.getData(red, green, blue);
        else {
            Integer c = new Integer(color);
            if (map.containsKey(c)) {
                String cs = map.get(c).toString();
                return cs + cs;
            }
        }
        return "";
    }

    /**
     * Just return a newline string.
     * @return Newline string.
     */
    public String getNextLineString() {
        return Info.LINE_END;
    }

    /**
     * As post processing we reset the twice value of the DataProvider, because
     * in plain text mode we always use the twice mode and have to reset it to
     * false. Also the character map is cleared so that the GC can collect it.
     * @param buf Not used.
     */
    public void postProcess(StringBuffer buf) {
        dataProvider.setTwice(false);
        map.clear();
    }

    /**
     * We write all already generated data to the output file and clear the
     * buffer.
     */
    public void flush(BufferedWriter bw, StringBuffer data) throws IOException {
        bw.write(data.toString());
        data.delete(0, data.length());
    }
}
