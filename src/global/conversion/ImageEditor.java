/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.Info;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * This class implements modification functions for an image.
 * @author xsdnyd
 */
public class ImageEditor {

    /** The image which gets modified. */
    private BufferedImage img;

    /** Constructs a new ImageEditor and the image is set to null. */
    public ImageEditor() {
        img = null;
    }

    /**
     * Tries to load the specified image.
     * @param file Imagefile.
     * @throws java.io.IOException 
     */
    public void setImage(File file) throws IOException {
        img = ImageIO.read(file);
    }

    /**
     * Returns the loaded image.
     * @return Loaded image.
     */
    public BufferedImage getImage() {
        return img;
    }

    /**
     * Returns the image width.
     * @return Image width or -1 if no valid image is loaded.
     */
    public int getWidth() {
        return (img == null ? -1 : img.getWidth());
    }

    /**
     * Returns the image height.
     * @return Image height or -1 if no valid image is loaded.
     */
    public int getHeight() {
        return (img == null ? -1 : img.getHeight());
    }

    /**
     * Returns the image aspect ratio.
     * @return Image aspect ratio or -1 if no valid image is loaded.
     */
    public double getRatio() {
        return (img == null ? -1 : (double) getWidth() / (double) getHeight());
    }

    /**
     * Resizes the image to the specified width and height.
     * @param width Target image width.
     * @param height Target image height.
     */
    public void resize(int width, int height) {
        if (img != null && width > 0 && height > 0) {
            BufferedImage buf = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = (Graphics2D) buf.getGraphics();
            g.scale((double) width / getWidth(), (double) height / getHeight());
            g.drawImage(img, 0, 0, null);
            img = buf;
        }
    }

    /** Converts the image colorspace to grayscale. */
    public void setGrayscale() {
        if (img != null)
            img = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY),
                    null).filter(img, null);
    }

    /**
     * Quantizes the image. First an array with scanlines is created.
     * This array gets quantized by the Quantize class (written by Adam Doppelt,
     * see Quantize.java for details).
     * An image out of the new palette is created and drawn.
     * @param quantizeColors The target color count.
     * @return The new calculated palette.
     */
    public int[] quantize(int quantizeColors) {
        int height = img.getHeight(), width = img.getWidth();
        int pixels[][] = new int[height][];

        for (int y = 0; y < img.getHeight(); y++)
            pixels[y] = img.getRGB(0, y, width, 1, null, 0, width);
        int palette[] = Quantize.quantizeImage(pixels, quantizeColors);

        int newPixels[] = new int[width * height];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                newPixels[y * width + x] = palette[pixels[y][x]];

        img.getGraphics().drawImage((new Component() {}).createImage(
                new MemoryImageSource(width, height, newPixels, 0, width)), 0, 0, null);
        return palette;
    }
}
