/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import global.misc.Controller;
import global.misc.ImageTransformationInfo;
import global.misc.Info;
import global.misc.OutputInfo;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 * Provides a container for a char and int array.
 * @author xsdnyd
 */
class ColorCharArray {

    /** Represents the characters corresponding to the scanline colors. */
    char chars[];
    /** Represents a scanline from the processed image. */
    int colors[];

    /**
     * Initializes the arrays with the specified ones.
     * @param chr The char array.
     * @param c The int array.
     */
    public ColorCharArray(char chr[], int c[]) {
        this.chars = chr;
        this.colors = c;
    }

    /**
     * Returns the int array.
     * @return Int array.
     */
    public int[] getColors() {
        return colors;
    }

    /**
     * Returns the char array.
     * @return Char array.
     */
    public char[] getChars() {
        return chars;
    }
}

/**
 * This class implements the conversion algorithms in a threaded way.
 * @author xsdnyd
 */
public class ConvertThread extends Thread {

    /** The language ressource which provides translated Strings. */
    private static final ResourceBundle lang =
            java.util.ResourceBundle.getBundle("global/resources/lang");
    /** 
     * The magic line value which is subtracted from the lineheight in image
     * output mode. This reduces the space between the character rows a little 
     * bit.
     */
    private final int MAGIC_LINE_VALUE = 3;
    /** The application controller. Provides the relevant data. */
    private Controller controller;
    /** The new image palette, if image was quantized. */
    private int[] palette;

    /**
     * Creates the conversion thread and initializes the controller.
     * @param controller Application controller.
     */
    public ConvertThread(Controller controller) {
        this.controller = controller;
        this.palette = null;
    }

    /**
     * Starts the conversion process. The input image is reloaded because it
     * might be modified by a previous conversion.
     * The image transformations are applied and the text or image output is
     * started.
     */
    @Override
    public void run() {
        controller.reloadImage();
        int outputFormat = controller.getOutputInfo().getOutputFormat();
        ImageTransformationInfo iti = controller.getTransformationInfo();
        if (iti.hasResizeInfo()) {
            controller.getUI().statusMessage(lang.getString("Progress_Resize"));
            controller.getImageEditor().resize(iti.getResizeWidth(),
                    iti.getResizeHeight());
        }
        if (iti.hasGrayscaleInfo()) {
            controller.getUI().statusMessage(lang.getString("Progress_Grayscale"));
            controller.getImageEditor().setGrayscale();
        }
        if (iti.hasQuantizeInfo() && outputFormat != OutputInfo.OUTPUT_TXT) {
            controller.getUI().statusMessage(lang.getString("Progress_Quantize"));
            palette = controller.getImageEditor().quantize(iti.getQuantizeColors());
        }
        if (outputFormat == OutputInfo.OUTPUT_TXT) {
            controller.getUI().statusMessage(lang.getString("Progress_Quantize"));
            palette = controller.getImageEditor().quantize(
                    Info.DETAILS[controller.getOutputInfo().getQuality()]);
        }
        if (outputFormat == OutputInfo.OUTPUT_HTML ||
                outputFormat == OutputInfo.OUTPUT_HTMLCSS ||
                outputFormat == OutputInfo.OUTPUT_RTF ||
                outputFormat == OutputInfo.OUTPUT_TXT)
            generateTextASCII();
        else
            generateImageASCII();
    }

    /**
     * Creates a TextOutput object depending on the selected text output mode.
     * @return Valid TextOutput object. Defaults to HTML output.
     */
    private TextOutput createTextOutput() {
        OutputInfo outputInfo = controller.getOutputInfo();
        switch (outputInfo.getOutputFormat()) {
            case OutputInfo.OUTPUT_HTML:
                return new HTMLOutput(outputInfo.getFont(),
                        controller.getDataProvider(), outputInfo.getBackColor());
            case OutputInfo.OUTPUT_HTMLCSS:
                return new CSSHTMLOutput(outputInfo.getFont(),
                        controller.getDataProvider(), outputInfo.getBackColor());
            case OutputInfo.OUTPUT_RTF:
                return new RTFOutput(outputInfo.getFont(),
                        controller.getDataProvider(), outputInfo.getBackColor(),
                        controller.getImageEditor().getImage());
            case OutputInfo.OUTPUT_TXT:
                return new PlainTextOutput(controller.getDataProvider(), palette);
            default:
                return new HTMLOutput(outputInfo.getFont(),
                        controller.getDataProvider(), outputInfo.getBackColor());
        }
    }

    /**
     * Processes each scanline of the input image.
     * Each pixel of the scanline is processed by the TextOutput object.
     * Afterwards the text file is written. If not already by the flush function
     * of the TextOutput object.
     */
    private void generateTextASCII() {
        TextOutput textOutput = createTextOutput();
        BufferedImage img = controller.getImageEditor().getImage();
        StringBuffer buf = new StringBuffer(textOutput.getHeader());

        int color, red, green, blue, update = 0;
        int width = controller.getImageEditor().getWidth(),
                height = controller.getImageEditor().getHeight();
        int scanline[];
        controller.getUI().setMaxProgress(height - 1);
        long time1 = System.currentTimeMillis();

        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(
                    controller.getOutputFile()));

            for (int y = 0; y < height; y++) {
                if (isCancelled())
                    return;
                update++;
                if (update % 20 == 0)
                    controller.getUI().setProgress(y);
                scanline = img.getRGB(0, y, width, 1, null, 0, width);
                for (int x = 0; x < scanline.length; x++) {
                    color = scanline[x];
                    red = (color >> 16) & 0xff;
                    green = (color >> 8) & 0xff;
                    blue = color & 0xff;
                    buf.append(textOutput.processPixel((x + 1) * (y + 1), red, green, blue, color));
                }
                buf.append(textOutput.getNextLineString());
                textOutput.flush(fileWriter, buf);
            }
            buf.append(textOutput.getFooter());
            controller.getUI().statusMessage(lang.getString("Progress_Post_Process"));
            textOutput.postProcess(buf);
            controller.getUI().statusMessage(lang.getString("Progress_Writing_File"));
            fileWriter.write(buf.toString());
            fileWriter.close();
            controller.finishedConversion();
        } catch (IOException ex) {
            controller.getUI().errorMessage(lang.getString("Error_Writing_File"), ex);
            return;
        }
        long time2 = System.currentTimeMillis();
        controller.getUI().statusMessage(String.format(lang.getString("Conversion_Finished"), time2 - time1));
    }

    /**
     * This function creates a ColorChar table containing all
     * image scanlines and the corresponding characters.
     * This is needed because in most fonts the characters have a different 
     * width and therefore the resulting ascii image has to have a width which
     * is the maximum width of a character line.
     * The twice value has to be temporarily set to false, otherwise the
     * custom text mode, will only display every second character.
     * @param img The input image.
     * @param dataProvider The provider of the ascii data.
     * @return A complete ColorChar array containing all scanlines and 
     * characters.
     */
    private ColorCharArray[] createColorCharTable(BufferedImage img,
            DataProvider dataProvider) {
        int width = img.getWidth(), height = img.getHeight();
        int red, green, blue;
        ColorCharArray charData[] = new ColorCharArray[height];
        int rowColors[];
        char rowChars[];
        boolean old_twice_val = dataProvider.isTwice();
        dataProvider.setTwice(false);
        for (int y = 0; y < height; y++) {
            if (isCancelled())
                return null;
            rowColors = img.getRGB(0, y, width, 1, null, 0, width);
            rowChars = new char[width];
            for (int x = 0; x < rowColors.length; x++)
                if (dataProvider.getMode() == DataProvider.DATA_BRIGHTNESSPATTERN) {
                    red = (rowColors[x] >> 16) & 0xff;
                    green = (rowColors[x] >> 8) & 0xff;
                    blue = rowColors[x] & 0xff;
                    rowChars[x] = dataProvider.getData(red, green, blue).charAt(0);
                } else
                    rowChars[x] = dataProvider.getData().charAt(0);
            charData[y] = new ColorCharArray(rowChars, rowColors);
        }
        dataProvider.setTwice(old_twice_val);
        return charData;
    }

    /**
     * Evaluates a ColorChar table and calculates the new width and height of
     * the output image. The image is created and returned.
     * @param img The input image.
     * @param font The output font.
     * @param charData The ColorChar table.
     * @return The new sized output image.
     */
    private BufferedImage createNewSizedImage(BufferedImage img, Font font,
            ColorCharArray charData[]) {
        Graphics2D g = (Graphics2D) img.getGraphics();
        int length = 0, newWidth = 0;
        for (ColorCharArray cca : charData) {
            length = g.getFontMetrics(font).charsWidth(cca.getChars(), 0, cca.getChars().length);
            if (length > newWidth)
                newWidth = length;
        }
        int lineHeight = g.getFontMetrics(font).getHeight() - MAGIC_LINE_VALUE;
        int newHeight = lineHeight * charData.length;
        return new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
    }

    /**
     * First the ColorChar table is calculated and the new sized image is 
     * created. Then each character of the ColorChar table is drawn to the
     * resulting image using the color in the scanline. Afterwards the image
     * file is written.
     */
    private void generateImageASCII() {
        BufferedImage img = controller.getImageEditor().getImage();
        OutputInfo outputInfo = controller.getOutputInfo();
        DataProvider dataProvider = controller.getDataProvider();

        controller.getUI().statusMessage(lang.getString("Progress_Generating_Ascii"));
        ColorCharArray charData[] = createColorCharTable(img, dataProvider);
        if (charData == null)
            return;

        controller.getUI().statusMessage(lang.getString("Progress_Creating_Image"));
        BufferedImage imgFile = createNewSizedImage(img, outputInfo.getFont(),
                charData);
        Graphics2D g = (Graphics2D) imgFile.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setFont(outputInfo.getFont());
        g.setColor(outputInfo.getBackColor());
        g.fillRect(0, 0, imgFile.getWidth(), imgFile.getHeight());

        int lineHeight = g.getFontMetrics().getHeight() - MAGIC_LINE_VALUE;
        int scanline[];
        char charline[];
        int update = 0, pos = 0;
        controller.getUI().setMaxProgress(charData.length - 1);
        long time1 = System.currentTimeMillis();

        for (int y = 0; y < charData.length; y++) {
            if (isCancelled())
                return;
            update++;
            if (update % 20 == 0)
                controller.getUI().setProgress(y);
            scanline = charData[y].getColors();
            charline = charData[y].getChars();
            pos = 0;
            for (int x = 0; x < scanline.length; x++) {
                g.setColor(new Color(scanline[x]));
                g.drawString("" + charline[x], pos, lineHeight * (y + 1));
                pos += g.getFontMetrics().charWidth(charline[x]);
            }
        }
        controller.getUI().statusMessage(lang.getString("Progress_Writing_File"));
        writeImageFile(controller.getOutputFile(), outputInfo.getOutputFormat(),
                (float) outputInfo.getQuality() / 10, imgFile);
        long time2 = System.currentTimeMillis();
        controller.getUI().statusMessage(String.format(lang.getString("Conversion_Finished"), time2 - time1));
        controller.finishedConversion();
    }

    /**
     * Saves an input image in the specified file. If the output format is jpg
     * the quality parameter is used to set the jpg quality.
     * The best quality is archieved by the value 1.0. The lowest quality by
     * a value near 0.0.
     * @param outputFile The output filename.
     * @param outputFormat The output file format.
     * @param quality The jpg quality. Only used if jpg output is chosen.
     * @param img The image which should be written.
     */
    private void writeImageFile(String outputFile, int outputFormat,
            float quality, BufferedImage img) {
        try {
            if (outputFormat == OutputInfo.OUTPUT_JPG) {
                Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
                if (iter.hasNext()) {
                    ImageWriter writer = (ImageWriter) iter.next();
                    ImageOutputStream ios = ImageIO.createImageOutputStream(new File(outputFile));
                    writer.setOutput(ios);
                    ImageWriteParam iwparam = writer.getDefaultWriteParam();
                    iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    iwparam.setCompressionQuality(quality);
                    writer.write(null, new IIOImage(img, null, null), iwparam);
                    writer.dispose();
                    ios.close();
                }
            } else
                ImageIO.write(img, (outputFormat == OutputInfo.OUTPUT_BMP ? "bmp" : "png"),
                        new File(outputFile));
        } catch (IOException e) {
            controller.getUI().errorMessage(lang.getString("Error_Writing_File"), e);
        }
    }

    /**
     * Checks wether the thread was interrupted and sets the status label if so.
     * @return True if the thread was interrupted.
     */
    private boolean isCancelled() {
        if (isInterrupted()) {
            controller.getUI().statusMessage(lang.getString("Conversion_Cancelled"));
            return true;
        }
        return false;
    }
}
