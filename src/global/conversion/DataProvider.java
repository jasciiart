/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.conversion;

import java.util.Random;

/**
 * This class provides the ascii data for the text output formats.
 * @author xsdnyd
 */
public class DataProvider {

    /** The constants specify the type ascii data generated. */
    public final static int DATA_BINARY = 0;
    public final static int DATA_NUMBERS = 1;
    public final static int DATA_SMALLCHARS = 2;
    public final static int DATA_BIGCHARS = 3;
    public final static int DATA_CUSTOMCHARS = 4;
    public final static int DATA_CUSTOMTEXT = 5;
    public final static int DATA_BRIGHTNESSPATTERN = 6;
    public final static int DATA_RANDOMCUSTOMTEXT = 10;
    /** The current ascii data mode. */
    private int mode;
    /** The current position in the custom text String. */
    private int currentCharPos;
    /** The custom text used in custom text and brightness mode. */
    private String text;
    /** The range of the custom characters. */
    private int customCharBegin,  customCharEnd;
    /** The random number generator. */
    private Random rnd;
    /** 
     * The twice value specifies if the getData function will return a string
     * of length 1 or 2.
     */
    private boolean twice;
    private int divider;

    /**
     * Constructs a DataProvider and sets the ascii data mode to binary numbers.
     * @param twice The twice value.
     */
    public DataProvider(boolean twice) {
        this.mode = DATA_BINARY;
        this.setTwice(twice);
        rnd = new Random();
    }

    /**
     * Returns ascii data. The brightness is calculated via a magic formula ;).
     * The three color components are used to calculate the brightness.
     * @param red The red color component.
     * @param green The green color component.
     * @param blue The blue color component.
     * @return Generated ascii data.
     */
    public final String getData(int red, int green, int blue) {
        int brightness = (77 * red + 150 * green + 29 * blue) / 256;
        if (isTwice())
            return "" + getDataChar(brightness) + getDataChar(brightness);
        else
            return Character.toString(getDataChar(brightness));
    }

    /**
     * Returns ascii data. This is typically not called if the mode is set to
     * DATA_BRIGHTNESSPATTERN because no RGB values are specified.
     * @return Generated ascii data.
     */
    public final String getData() {
        return getData(0, 0, 0);
    }

    /**
     * Sets the ascii data mode to the specified one.
     * @param mode Ascii data mode.
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * Returns the current ascii data mode.
     * @return Ascii data mode.
     */
    public int getMode() {
        return mode;
    }

    /**
     * Sets the custom text to the specified text and calculates the
     * divider value.
     * @param text The new text.
     */
    public void setText(String text) {
        this.text = text;
        divider = (255 / (text.length() - 1));
        currentCharPos = -1;
    }

    /**
     * Sets the begin of the custom char range.
     * @param begin The begin of the range.
     */
    public void setCustomCharBegin(int begin) {
        this.customCharBegin = begin;
    }

    /**
     * Sets the end of the custom char range.
     * @param end The end of the range.
     */
    public void setCustomCharEnd(int end) {
        this.customCharEnd = end;
    }

    /**
     * Sets the twice value to the given parameter.
     * @param twice New twice value.
     */
    public void setTwice(boolean twice) {
        this.twice = twice;
    }

    /**
     * Returns the current twice value.
     * @return Twice value.
     */
    public boolean isTwice() {
        return twice;
    }

    /**
     * Returns the maximum different characters the current ascii data mode
     * can over. This is used in the plain text format to check wether each
     * color can have a different character.
     * @return Count of available characters.
     */
    public final int getMutation() {
        switch (mode) {
            case DATA_BINARY:
                return 2;
            case DATA_NUMBERS:
                return 10;
            case DATA_SMALLCHARS:
                return 26;
            case DATA_BIGCHARS:
                return 26;
            case DATA_CUSTOMCHARS:
                return customCharEnd - customCharBegin;
            case DATA_CUSTOMTEXT:
                return text.length();
            default:
                return 0;
        }
    }

    /**
     * This function generates the ascii char depending on the current mode.
     * @param brightness The brightness value, used in the 
     * DATA_BRIGHTNESSPATTERN mode.
     * @return Generated ascii char.
     */
    private char getDataChar(int brightness) {
        switch (mode) {
            case DATA_BINARY:
                return rnd.nextBoolean() ? '1' : '0';
            case DATA_NUMBERS:
                return (char) ((int) '0' + rnd.nextInt(10));
            case DATA_SMALLCHARS:
                return (char) ((int) 'a' + rnd.nextInt(26));
            case DATA_BIGCHARS:
                return (char) ((int) 'A' + rnd.nextInt(26));
            case DATA_CUSTOMCHARS:
                return (char) (customCharBegin + rnd.nextInt(customCharEnd - customCharBegin));
            case DATA_CUSTOMTEXT: {
                if (++currentCharPos > text.length() - 1)
                    currentCharPos = 0;
                return text.charAt(currentCharPos);
            }
            case DATA_RANDOMCUSTOMTEXT:
                return text.charAt(rnd.nextInt(text.length()));
            case DATA_BRIGHTNESSPATTERN: {
                int index = (int) Math.floor(brightness / divider);
                if (index >= text.length())
                    index = text.length() - 1;
                else if (index < 0)
                    index = 0;
                return text.charAt(index);
            }
            default:
                return '0';
        }
    }
}
