/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

/**
 * Defines all functions that a valid user interface (gui and cui) has to
 * provide, to be accepted by the application controller.
 * @author xsdnyd
 */
public interface UserInterface {

    /**
     * This is called when an error message should be shown.
     * @param message The error message.
     * @param e The Exception itself.
     */
    public void errorMessage(String message, Exception e);

    /**
     * The current status message which indicated the current process.
     * @param message The current process.
     */
    public void statusMessage(String message);

    /** This function is called when the conversion is started. */
    public void startedConversion();

    /** This function is called when the conversion is finished. */
    public void finishedConversion();

    /** 
     * This function should return the maximum progress which can be
     * reached via the setProgress function.
     * @return The maximum progress.
     */
    public int getMaxProgress();

    /**
     * Sets the maximum progress to the given value.
     * @param progress The new maximum progress.
     */
    public void setMaxProgress(int progress);

    /**
     * Returns the current progress.
     * @return Current progress.
     */
    public int getProgress();

    /**
     * Sets the current progress to the given value.
     * @param progress New current progress.
     */
    public void setProgress(int progress);

    /**
     * Returns the current step in a wizard. Certainly not used in a commandline
     * userinterface.
     * @return The current wizard step.
     */
    public int getStep();

    /**
     * Returns the maximum wizard step the userinterface can have.
     * @return The maximum wizard step which is valid.
     */
    public int getMaxSteps();

    /**
     * Sets the current wizard step to the given value.
     * @param step The new current wizard step.
     */
    public void setStep(int step);
}
