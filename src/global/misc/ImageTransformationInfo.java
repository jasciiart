/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

/**
 * This component just stores some information for the input image 
 * transformation options.
 * @author xsdnyd
 */
public class ImageTransformationInfo {

    /** Boolean values wether the actions have to be done. */
    private boolean resize,  grayscale,  quantize;
    /** The desired width, height and color count of the image. */
    private int width,  height,  colors;

    /**
     * Creates a new ImageTransformationInfo component with the specified values.
     * @param resize Wether the image should be resized.
     * @param width The target width of the image.
     * @param height The target height of the image.
     * @param grayscale Wether the image color space should be converted to
     * grayscale.
     * @param quantize Wether to quantize the image.
     * @param colors The target color count.
     */
    public ImageTransformationInfo(boolean resize, int width, int height,
            boolean grayscale, boolean quantize, int colors) {
        this.resize = resize;
        this.width = width;
        this.height = height;
        this.grayscale = grayscale;
        this.quantize = quantize;
        this.colors = colors;
    }

    /**
     * Returns wether the image should be resized.
     * @return True if the image should be resized.
     */
    public boolean hasResizeInfo() {
        return resize;
    }

    /**
     * Returns the target image width.
     * @return Target width.
     */
    public int getResizeWidth() {
        return width;
    }

    /**
     * Returns the target image height.
     * @return Target height.
     */
    public int getResizeHeight() {
        return height;
    }

    /**
     * Returns wether the image color space should be converted to grayscale.
     * @return True if image should be converted to grayscale.
     */
    public boolean hasGrayscaleInfo() {
        return grayscale;
    }

    /**
     * Returns wether the image should be quantized to the target color count.
     * @return True if image should be quantized.
     */
    public boolean hasQuantizeInfo() {
        return quantize;
    }

    /**
     * Returns the target amount of colors in the image.
     * @return Target color count.
     */
    public int getQuantizeColors() {
        return colors;
    }
}
