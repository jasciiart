/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

import java.awt.Color;
import javax.swing.UIManager;

/**
 * This class contains static constants that are widely used
 * by other classes.
 * @author xsdnyd
 */
public abstract class Info {

    public final static String APP_NAME = "JASCiiArt";
    public final static String APP_VERSION = "0.30.beta2";
    public final static String COPYRIGHT = "Copyright (C) 2008 Markus Gross";
    public final static String HOME_PAGE = "http://reactor.reality-protocol.de";
    public final static String LINE_END = System.getProperty("line.separator");
    public final static String FILE_SEPARATOR = System.getProperty("file.separator");
    /** This color is used to draw a border in JColorPanel and JImagePanel. */
    public final static Color BORDER_COLOR = UIManager.getColor("controlDkShadow");
    /**
     * These are the color counts for the different detail levels of plain text
     * output.
     */
    public final static int DETAILS[] = {5, 10, 20, 30, 40, 50, 75, 100, 150, 200};
    /** 
     * A hashmap needs an initial value that is usually filled up to 75% to
     * provide speed. So if we know the exact value we multiply it with our
     * load factor. This will give us exactly 75%.
     */
    public final static float HASHMAP_LOAD_FACTOR_INVERTED = 1.25f;

    private Info() {
    }
}

