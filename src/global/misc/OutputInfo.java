/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

import java.awt.Color;
import java.awt.Font;

/**
 * This component just stores some information about the output format.
 * @author xsdnyd
 */
public class OutputInfo {

    /** Valid output formats. */
    public final static int OUTPUT_HTML = 0;
    public final static int OUTPUT_HTMLCSS = 1;
    public final static int OUTPUT_RTF = 2;
    public final static int OUTPUT_TXT = 3;
    public final static int OUTPUT_BMP = 4;
    public final static int OUTPUT_JPG = 5;
    public final static int OUTPUT_PNG = 6;
    /** The extensions according to the output formats. */
    public final static String[] extensions = {"html", "html", "rtf", "txt",
        "bmp", "jpg", "png"
    };
    private int outputFormat;
    /** 
     * This field is used to store the detail level if plain text output is 
     * chosen or to store the jpg quality.
     * If jpg output is chosen this value has to be divided by 10 and type
     * casted to a float.
     */
    private int quality;
    /** Background color of the output ascii art. */
    private Color backColor;
    /** Stores the output font. */
    private Font font;

    /**
     * Creates a new OutputInfo component and sets all values.
     * @param font The output font.
     * @param backColor Background color.
     * @param outputFormat Output format.
     * @param quality The detail level if plain text is chosen or quality if jpg
     * output is chosen.
     */
    public OutputInfo(Font font, Color backColor, int outputFormat, int quality) {
        this.font = font;
        this.backColor = backColor;
        this.outputFormat = outputFormat;
        this.quality = quality;
    }

    /**
     * Returns the output format.
     * @return Output format.
     */
    public int getOutputFormat() {
        return outputFormat;
    }

    /**
     * Returns the output font.
     * @return Output font.
     */
    public Font getFont() {
        return font;
    }

    /**
     * Returns the background color.
     * @return Background color.
     */
    public Color getBackColor() {
        return backColor;
    }

    /**
     * Returns the quality of the jpg output or the detail level of the plain
     * text output.
     * @return Quality or Detail level.
     */
    public int getQuality() {
        return quality;
    }
}
