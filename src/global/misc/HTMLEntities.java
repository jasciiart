/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

/**
 * Creates an array containing all html entities and if a character doesn't has
 * an entity it contains the character as string itself. This array is used in 
 * the HTML output file format to display special characters.
 * @author xsdnyd
 */
public class HTMLEntities {

    /**
     * The entity array. The array length is 256 for the whole ascii palette.
     * The entity to each character can be obtained by choosing the
     * ordinal value of the character as array index.
     * The characters which don't have an entity just have their String
     * representation as entity.
     */
    private String entities[];

    /** Creates a new HTML entity array and fills it. */
    public HTMLEntities() {
        entities = new String[256];
        addCharRangeWithoutEntity(0, 33);
        addChar(34, "&quot;");
        addCharRangeWithoutEntity(35, 37);
        addCharRangeWithEntity(38, 39, new String[]{"&amp;", "&apos;"});
        addCharRangeWithoutEntity(40, 59);
        addChar(60, "&lt;");
        addCharRangeWithoutEntity(61, 61);
        addChar(62, "&gt;");
        addCharRangeWithoutEntity(63, 159);
        addCharRangeWithEntity(160, 255, new String[]{
                    "&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;",
                    "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;",
                    "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;",
                    "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;",
                    "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;",
                    "&iquest;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;",
                    "&Aring;", "&AElig;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;",
                    "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;",
                    "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;",
                    "&times;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;",
                    "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;",
                    "&atilde;", "&auml;", "&aring;", "&aelig;", "&ccedil;", "&egrave;",
                    "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;",
                    "&iuml;", "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;",
                    "&otilde;", "&ouml;", "&divide;", "&oslash;", "&ugrave;",
                    "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;"
                });
    }

    /**
     * Adds a range of characters with their entities to the entity array.
     * The String array containing the entities to add, should have the length
     * max - min.
     * @param min The lower bound of the range to add.
     * @param max The upper bound of the range to add.
     * @param entities The entity array containing the entity Strings to add.
     */
    private void addCharRangeWithEntity(int min, int max, String entities[]) {
        for (int i = min; i <= max && i < (min + entities.length); i++)
            addChar(i, entities[i - min]);
    }

    /**
     * Adds an entity to the entity array.
     * The corresponding character to the entity is the array index position.
     * @param val Corresponding character / array index position.
     * @param entity Entity String.
     */
    private void addChar(int val, String entity) {
        entities[val] = entity;
    }

    /**
     * Adds a range of characters with no entity to the entity array.
     * The entity is the character itself as String.
     * @param min The lower bound of the range.
     * @param max The upper bound of the range.
     */
    private void addCharRangeWithoutEntity(int min, int max) {
        for (int i = min; i <= max; i++)
            addChar(i, Character.toString((char) i));
    }

    /**
     * Looks up the entity of a character and returns it.
     * If the character doesn't have an entity the character as string is
     * returned.
     * @param c Character to lookup.
     * @return The entity string.
     */
    public String getEntity(char c) {
        return entities[c];
    }
}
