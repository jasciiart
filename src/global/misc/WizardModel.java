/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

import java.util.LinkedList;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Represents a model to check wether the selected step is allowed.
 * @author xsdnyd
 */
public class WizardModel implements SingleSelectionModel {

    /** Current selected page. **/
    private int selected;
    /** Needed to check wether step is allowed and to handle it. */
    private Controller controller;
    /** List of listeners. */
    private LinkedList<ChangeListener> l;

    /**
     * Creates a new WizardModel with specified controller.
     * @param controller The application controller.
     */
    public WizardModel(Controller controller) {
        this.controller = controller;
        selected = 0;
        l = new LinkedList<ChangeListener>();
    }

    /**
     * Just returns the current index.
     * @return Current index.
     */
    public int getSelectedIndex() {
        return selected;
    }

    /**
     * Asks controller if the step is valid.
     * If the step is valid, the index is updated and a ChangeEvent is
     * sent to all listeners.
     * @param index Target wizard step.
     */
    public void setSelectedIndex(int index) {
        if (controller.canStep(getSelectedIndex(), index)) {
            selected = index;
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener cl : l)
                cl.stateChanged(e);
        }
    }

    /**
     * Resets the current index to -1.
     */
    public void clearSelection() {
        selected = -1;
    }

    /**
     * A valid selection is made when it is greater than -1.
     * @return Returns true is current index is greater than -1.
     */
    public boolean isSelected() {
        return selected != -1;
    }

    /**
     * Adds a listener to the listeners list.
     * @param listener Listener to add.
     */
    public void addChangeListener(ChangeListener listener) {
        l.add(listener);
    }

    /**
     * Removes a listener from the listeners list.
     * @param listener Listener to remove.
     */
    public void removeChangeListener(ChangeListener listener) {
        l.remove(listener);
    }
}
