/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

import global.conversion.ConvertThread;
import global.conversion.DataProvider;
import global.conversion.ImageEditor;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

/**
 * This is the application controller.
 * It contains application logic and manages all important data objects.
 * @author xsdnyd
 */
public class Controller {

    /** Valid wizard steps. */
    public final static int STEP_INPUT = 0;
    public final static int STEP_PATTERNDESIGN = 1;
    public final static int STEP_OUTPUT = 2;
    public final static int STEP_SUMMARY = 3;
    /** The language ressource which provides translated Strings. */
    private static final ResourceBundle lang =
            java.util.ResourceBundle.getBundle("global/resources/lang");
    /** The image editor to modify the input image. */
    private ImageEditor imageEditor;
    /** Transformation info to store what should be modified in the input image. */
    private ImageTransformationInfo transformationInfo;
    /** Provides the ascii data to create the ascii picture of. */
    private DataProvider dataProvider;
    /** Output info which format and options the output ascii picture should have. */
    private OutputInfo outputInfo;
    /** The thread which does the conversion. */
    private ConvertThread convertThread;
    /** The input and output files. */
    private File inputfile,  outputfile;
    /** The user interface .*/
    private UserInterface ui;

    /**
     * Creates a new application controller and sets the specified variables.
     * @param ui The class implementing a user interface.
     */
    public Controller(UserInterface ui) {
        imageEditor = new ImageEditor();
        inputfile = null;
        dataProvider = null;
        transformationInfo = null;
        outputInfo = null;
        this.ui = ui;
    }

    /**
     * Tries to load the image and set the input file.
     * @param file Image-file to load.
     * @return True if the file was loaded successfully.
     */
    public boolean loadImage(File file) {
        try {
            imageEditor.setImage(file);
            inputfile = file;
            return true;
        } catch (IOException e) {
            inputfile = null;
            ui.errorMessage(lang.getString("Error_Loading_Image_Failed"), e);
            return false;
        }
    }

    /**
     * Returns the currently loaded image.
     * @return Currently loaded image.
     */
    public Image getImage() {
        return imageEditor.getImage();
    }

    /**
     * Returns the filename of the input file.
     * @return Input file filename.
     */
    public String getInputFile() {
        return (inputfile == null ? lang.getString("Error_Invalid_Image") : inputfile.toString());
    }

    /**
     * Returns the filename of the output file.
     * @return Output file filename.
     */
    public String getOutputFile() {
        return (outputfile == null ? null : outputfile.toString());
    }

    /** Reloads the already loaded (but probably modified) input image. */
    public void reloadImage() {
        if (inputfile != null)
            loadImage(inputfile);
    }

    /**
     * Tests wether the input file is valid.
     * An input file is valid if it is a valid image and can be read.
     * @return True if the input file is valid.
     */
    private boolean verifyInput() {
        if (inputfile == null || !inputfile.canRead()) {
            ui.errorMessage(lang.getString("Error_Invalid_Input"), null);
            return false;
        }
        return true;
    }

    /**
     * Tests wether the output file is valid.
     * An output file is valid if the file doesn't exist or can be overwritten.
     * @return True if the output file is valid.
     */
    private boolean verifyOutput() {
        try {
            if (outputfile == null || (!outputfile.canWrite() && outputfile.exists()))
                throw new IOException(lang.getString("Error_Invalid_Output"));
            if (!outputfile.exists()) {
                outputfile.createNewFile();
                outputfile.delete();
            }
            return true;
        } catch (IOException ex) {
            ui.errorMessage(lang.getString("Error_Invalid_Output"), ex);
            return false;
        }
    }

    /**
     * Determines wether the target wizard step if valid from the current
     * wizard step.
     * @param current Current wizard step.
     * @param target Target wizard step.
     * @return True if the jump is valid.
     */
    public boolean canStep(int current, int target) {
        if (current < 0 || target > ui.getMaxSteps() ||
                current == target)
            return false;
        if (target == STEP_SUMMARY) {
            if (current == STEP_INPUT && !verifyInput())
                return false;
            if (!verifyOutput()) {
                ui.setStep(STEP_OUTPUT);
                return false;
            }
            return true;
        } else if (target == STEP_PATTERNDESIGN || target == STEP_OUTPUT) {
            if (!verifyInput())
                return false;
            return true;
        }
        return true;
    }

    /** Tries to go to the next wizard step. */
    public void nextStep() {
        if (canStep(ui.getStep(), ui.getStep() + 1))
            ui.setStep(ui.getStep() + 1);
    }

    /** Goes one step back in the wizard. */
    public void previousStep() {
        if (ui.getStep() > 0)
            ui.setStep(ui.getStep() - 1);
    }

    /** 
     * Sets the output file to specified file.
     * @param file The output is written to this file.
     */
    public void setOutputFile(File file) {
        outputfile = file;
    }

    /** Stops the conversion process and exits the application. */
    public void exit() {
        stopConversion();
        System.exit(0);
    }

    /**
     * Returns current ImageTransformationInfo.
     * @return Current ImageTransformationInfo.
     */
    public ImageTransformationInfo getTransformationInfo() {
        return transformationInfo;
    }

    /** 
     * Sets ImageTransformationInfo to specified one.
     * @param transformationInfo The new image transformation info.
     */
    public void setTransformationInfo(ImageTransformationInfo transformationInfo) {
        this.transformationInfo = transformationInfo;
    }

    /**
     * Returns current ImageEditor.
     * @return Current ImageEditor.
     */
    public ImageEditor getImageEditor() {
        return imageEditor;
    }

    /**
     * Returns current DataProvider.
     * @return Current DataProvider.
     */
    public DataProvider getDataProvider() {
        return dataProvider;
    }

    /** 
     * Sets DataProvider to specified one.
     * @param dp The new DataProvider which is used to generate the ascii data.
     */
    public void setDataProvider(DataProvider dp) {
        dataProvider = dp;
    }

    /**
     * Returns the current output info.
     * @return Current output info.
     */
    public OutputInfo getOutputInfo() {
        return outputInfo;
    }

    /**
     * Sets the output info to specified output info.
     * @param outputInfo The target output info.
     */
    public void setOutputInfo(OutputInfo outputInfo) {
        this.outputInfo = outputInfo;
    }

    /** After some validation checks the conversion process is started. */
    public void startConversion() {
        if (getDataProvider() == null)
            ui.errorMessage(lang.getString("Error_No_Data_Provider"), null);
        else if (getOutputInfo() == null)
            ui.errorMessage(lang.getString("Error_No_Output_Options"), null);
        else {
            ui.startedConversion();
            convertThread = new ConvertThread(this);
            convertThread.start();
        }
    }

    /** This tries to stop the conversion process. */
    public void stopConversion() {
        if (convertThread != null) {
            convertThread.interrupt();
            finishedConversion();
        }
    }

    /** This updates the component states when the conversion finished .*/
    public void finishedConversion() {
        convertThread = null;
        ui.finishedConversion();
    }

    /**
     * Returns the userinterface.
     * @return Userinterface.
     */
    public UserInterface getUI() {
        return ui;
    }
}
