/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package global.misc;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Implements the file dialog filter.
 * @author xsdnyd
 */
public class FileDialogFilter extends FileFilter {

    /** Allowed extensions. */
    private String filters[];
    /** Description for the filter. */
    private String description;

    /**
     * Constructs the file filter object and initializes the variables.
     * @param filters The allowed file extensions.
     * @param description The description for the filter.
     */
    public FileDialogFilter(String filters[], String description) {
        this.filters = filters;
        this.description = description;
    }

    /**
     * If the file has a valid extension the file is accepted.
     * @param file File to validate.
     * @return True if the file is accepted.
     */
    public boolean accept(File file) {
        if (file != null) {
            if (file.isDirectory())
                return true;
            String ext = getExtension(file);
            if (ext != null)
                for (String filter : filters)
                    if (ext.equalsIgnoreCase(filter))
                        return true;
        }
        return false;
    }

    /**
     * Returns the extension of an file.
     * @param file The input file.
     * @return The extension of the file.
     */
    public String getExtension(File file) {
        if (file != null) {
            String filename = file.getName();
            int i = filename.lastIndexOf('.');
            if ((i > 0) && (i < filename.length() - 1))
                return filename.substring(i + 1);
        }
        return null;
    }

    /**
     * Returns the filters description.
     * @return The filters description.
     */
    public String getDescription() {
        return description;
    }
}
