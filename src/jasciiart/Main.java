/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package jasciiart;

import ui.console.CMain;
import ui.graphical.forms.JMain;

/**
 * This class just starts the application.
 * @author xsdnyd
 */
public class Main {

    /**
     * This function creates a new JMain Form and displays it.
     * Before that the Anti-Aliasing is turned on.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length > 0)
            new CMain(args);
        else {
            System.setProperty("awt.useSystemAAFontSettings", "on");
            java.awt.EventQueue.invokeLater(new Runnable() {

                public void run() {
                    new JMain().setVisible(true);
                }
            });
        }
    }

    private Main() {
    }
}
