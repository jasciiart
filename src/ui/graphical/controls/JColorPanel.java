/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package ui.graphical.controls;

import global.misc.Info;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JColorChooser;
import javax.swing.JComponent;

/**
 * This component draws a colored rect with a border around it.
 * @author xsdnyd
 */
public class JColorPanel extends JComponent implements MouseListener {

    /** The color of the component. */
    private Color backColor;

    /**
     * Creates a JColorPanel with default color WHITE.
     */
    public JColorPanel() {
        this(Color.WHITE);
    }

    /**
     * Create a JColorPanel with specified color as background color.
     * @param color Background color.
     */
    public JColorPanel(Color color) {
        addMouseListener(this);
        setBackColor(color);
        repaint();
    }

    /**
     * Draws the background color with border around it.
     * @param g Graphic context.
     */
    @Override
    public void paint(Graphics g) {
        g.setColor(Info.BORDER_COLOR);
        g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        g.setColor(getBackColor());
        g.fillRect(1, 1, getWidth() - 2, getHeight() - 2);
    }

    /**
     * Changes the background color.
     * @param backColor New background color.
     */
    public void setBackColor(Color backColor) {
        this.backColor = backColor;
        repaint();
    }

    /**
     * Returns the current background color
     * @return Current Background color.
     */
    public Color getBackColor() {
        return backColor;
    }

    /**
     * On a click a JColorChooser will be displayed to choose a color.
     * The selected color will be the new background color.
     * @param e Argument is ignored.
     */
    public void mouseClicked(MouseEvent e) {
        Color c = JColorChooser.showDialog(this, "select a background color",
                getBackColor());
        if (c != null)
            setBackColor(c);
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mouseExited(MouseEvent e) {
    }
}
