/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package ui.graphical.controls;

import global.misc.Info;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;

/**
 * This component displays the normal copyright info and the homepage when
 * hovered.
 * @author xsdnyd
 */
public class JCopyrightLabel extends JLabel implements MouseListener {

    /** The text which is displayed when the label is hovered. */
    private final String hovered = String.format(
            "<html><font color=\"#0000FF\">%s</font></html>",
            Info.HOME_PAGE);

    /** Creates a new copyright label with normal text. */
    public JCopyrightLabel() {
        setText(Info.COPYRIGHT);
        addMouseListener(this);
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Currently unused.
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Sets the text to display the homepage.
     * @param e Not used.
     */
    public void mouseEntered(MouseEvent e) {
        setText(hovered);
        repaint();
    }

    /**
     * Sets the text back to normal.
     * @param e Not used.
     */
    public void mouseExited(MouseEvent e) {
        setText(Info.COPYRIGHT);
        repaint();
    }
}
