/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package ui.graphical.controls;

import global.misc.Info;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JComponent;

/**
 * This component displays a picture with a border around it.
 * @author xsdnyd
 */
public class JImagePanel extends JComponent {

    /** The displayed picture itself. */
    private Image image;
    /** Aspect ratio of the picture. */
    private double ratio;
    /** Defines the maximum width and height of this component. */
    private final static int MAX_SIZE = 96;

    /**
     * Creates a new JImagePanel with no picture inside.
     */
    public JImagePanel() {
        this(null);
    }

    /**
     * Creates a new JImagePanel with specified picture inside.
     * @param image The picture which should be displayed.
     */
    public JImagePanel(Image image) {
        super();
        setImage(image);
        setMinimumSize(new Dimension(MAX_SIZE, MAX_SIZE));
        setPreferredSize(new Dimension(MAX_SIZE, MAX_SIZE));
    }

    /**
     * Sets the displayed picture. The aspect ratio is calculated and the
     * component repaints itself.
     * @param image The picture which should be displayed.
     */
    public void setImage(Image image) {
        this.image = image;
        ratio = (image == null ? 1 : (double) image.getWidth(null) /
                (double) image.getHeight(null));
        repaint();
    }

    /**
     * Calculates a new dimension specifying the width and height of the
     * border (hence the -1).
     * @return The calculated dimension.
     */
    private Dimension calcDrawSize() {
        if (image == null)
            return new Dimension(getWidth() - 1, getHeight() - 1);
        else if (image.getWidth(null) >= image.getHeight(null))
            return new Dimension(getWidth() - 1, (int) (getHeight() / ratio) - 1);
        else
            return new Dimension((int) (getWidth() * ratio) - 1, getHeight() - 1);
    }

    /**
     * Draws the border and picture.
     * @param g Graphic context.
     */
    @Override
    public void paint(Graphics g) {
        g.setColor(Info.BORDER_COLOR);
        Dimension drawSize = calcDrawSize();
        g.drawRect(0, 0, drawSize.width, drawSize.height);
        if (image != null)
            g.drawImage(image, 1, 1, drawSize.width - 1, drawSize.height - 1, null);
    }
}
