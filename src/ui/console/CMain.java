/*
 * Copyright (C) 2008 Markus Gross <xsdnyd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package ui.console;

import global.misc.Controller;
import global.misc.ImageTransformationInfo;
import global.misc.Info;
import global.misc.OutputInfo;
import global.misc.UserInterface;
import java.io.File;
import java.util.ResourceBundle;

/**
 * This class controls the Console UI.
 * @author xsdnyd
 */
public class CMain implements UserInterface {

    /** Stores the commandline arguments. */
    private String args[];
    /** The application controller. */
    private Controller controller;
    /** The language ressource which provides translated Strings. */
    private static final ResourceBundle lang =
            java.util.ResourceBundle.getBundle("global/resources/lang");
    /** Stores the image transformation which should be performed. */
    private ImageTransformationInfo imageTransformationInfo;
    /** Stores the ouput format info. */
    private OutputInfo outputInfo;
    /** The usage information. */
    private final static String usageInfo =
            lang.getString("Usage") + " java -jar jasciiart.jar [-h] [-v] " +
            "[-i=INPUTFILE -o=OUTPUTFILE] [-of=OUTPUTFORMAT] [-m=ASCIIMODE] [-d=NUMBER]" +
            "[-fn=FONTNAME] [-fs=FONTSIZE] [-fw=FONTWEIGHT] [-bg=COLOR] [-w=WIDTH]" +
            "[-h=HEIGHT] [-gs] [-q=NUMBER]\n\n" +
            "  -h             " + lang.getString("Desc_Usage") + "\n" +
            "  -v             " + lang.getString("Desc_Version") + "\n" +
            "  -i=INPUTFILE   " + lang.getString("Desc_Inputfile") + "\n" +
            "  -o=OUTPUTFILE  " + lang.getString("Desc_Outputfile") + "\n" +
            "  -of=FORMAT     " + lang.getString("Desc_OutputFormat") +
            "                 TXT     " + lang.getString("Output_TXT") + ",\n" +
            "                 HTML    " + lang.getString("Output_HTML") + ",\n" +
            "                 HTMLCSS " + lang.getString("Output_CSSHTML") + ",\n" +
            "                 RTF     " + lang.getString("Output_RTF") + ",\n" +
            "                 BMP     " + lang.getString("Output_BMP") + ",\n" +
            "                 JPG     " + lang.getString("Output_JPG") + ",\n" +
            "                 PNG     " + lang.getString("Output_PNG") + ".\n" +
            "  -d=NUMBER      " + lang.getString("Desc_Detail") + "\n" +
            "  -m=ASCIIMODE   " + lang.getString("Desc_AsciiMode") + "\n" +
            "                 BINARY  " + lang.getString("Ascii_Binary") + ",\n" +
            "                 NUMBERS " + lang.getString("Ascii_Numbers") + ",\n" +
            "                 LCHARS  " + lang.getString("Ascii_Lowercase_Chars") + ",\n" +
            "                 UCHARS  " + lang.getString("Ascii_Uppercase_Chars") + ",\n" +
            "                 RCHARS  " + lang.getString("Ascii_Range_Chars") + ",\n" +
            "                 TEXT    " + lang.getString("Ascii_Custom_Text") + ",\n" +
            "                 RTEXT   " + lang.getString("Ascii_Custom_Text") + " (" + lang.getString("Select_Random") + "),\n" +
            "                 BRIGHTN " + lang.getString("Ascii_Brightness_Pattern") + ".\n" +
            "  -ct=\"TEXT\"     " + lang.getString("Desc_Text") + "\n" +
            "  -fn=FONTNAME   " + lang.getString("Desc_Fontname") + "\n" +
            "  -fs=FONTSIZE   " + lang.getString("Desc_Fontsize") + "\n" +
            "  -fb            " + lang.getString("Desc_Fontbold") + "\n" +
            "  -bg=COLOR      " + lang.getString("Desc_Background") + "\n" +
            "  -w=WIDTH       " + lang.getString("Desc_Width") + "\n" +
            "  -h=HEIGHT      " + lang.getString("Desc_Height") + "\n" +
            "  -gs            " + lang.getString("Desc_Grayscale") + "\n" +
            "  -q=NUMBER      " + lang.getString("Desc_Quantize");

    /**
     * Parses the commandline arguments, verify the input commands and starts
     * the conversion process.
     * @param args Commandline arguments.
     */
    public CMain(String args[]) {
        this.args = args;
        System.out.println(Info.APP_NAME + " " + Info.APP_VERSION + " " + Info.COPYRIGHT);
        System.out.println(Info.HOME_PAGE + "\n");
        if (hasSwitch("-v")) {
            System.out.println(lang.getString("Version") + Info.APP_VERSION);
            System.exit(0);
        } else if (hasSwitch("-h")) {
            System.out.println(usageInfo);
            System.exit(0);
        }
        controller = new Controller(this);
        parseArguments();
    }

    /** Parses the entered commandline parameters. */
    private void parseArguments() {
        for (String arg : args)
            if (arg.startsWith("-i="))
                parseInputFile(getValue(arg));
    }

    /**
     * Returns wether a specified switch is in the argument list or not.
     * @param val The switch to be searched.
     * @return True if the switch is in the argument list, otherwise: False.
     */
    private boolean hasSwitch(String val) {
        for (String arg : args)
            if (val.equals(arg))
                return true;
        return false;
    }

    /**
     * Returns the value of a commandline argument.
     * Since commandline argument with an value looks like this:
     * key=value, we simply search the first = and return the rest of the String.
     * @param arg Commandline argument.
     * @return The value of the argument.
     */
    private String getValue(String arg) {
        return arg.substring(arg.indexOf("=") + 1);
    }

    /**
     * Just loads the specified input file.
     * @param value The input file.
     */
    private void parseInputFile(String value) {
        controller.loadImage(new File(value));
    }

    public void errorMessage(String message, Exception e) {
        System.err.println(lang.getString("ERROR") + " " + message);
        controller.exit();
    }

    public void statusMessage(String message) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void startedConversion() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void finishedConversion() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getMaxProgress() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setMaxProgress(int progress) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getProgress() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setProgress(int progress) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getStep() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getMaxSteps() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setStep(int step) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
